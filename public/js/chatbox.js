var chatbox = function()
{
	socket.emit('chatbox:ready');
	
	//****** DOM handlers ******///

	// Enter chatbox post
	$('#enterChatbox').submit(function() 
	{
		$("#alert").removeClass();
		$("#alert").html('');
	 	socket.emit('chatbox:enter', $('#enterChatbox fieldset input[name="password"]').val());
		return false;
	});

	$('#enterChatbox fieldset span.button').bind('click', function(e)
	{
		$("#alert").removeClass();
		$("#alert").html('');
		socket.emit('chatbox:enter', $('#enterChatbox fieldset input[name="password"]').val());
		return false;
	});

	// Nickname post
	$('#nicknameForm').submit(function() 
	{
	 	socket.emit('chatbox:setNickname', $('#nicknameForm fieldset input[name="nickname"]').val());
		return false;
	});

	$('#nicknameForm fieldset span.button').bind('click', function(e)
	{
		socket.emit('chatbox:setNickname', $('#nicknameForm fieldset input[name="nickname"]').val());
		return false;
	});

	// Chatbox Post
	$('.chatboxPost').submit(function(){
		socket.emit('chatbox:post', $('.chatboxPost fieldset input[name="message"]').val())
		$('.chatboxPost fieldset input[name="message"]').val('');
		return false;
	});
	$('.chatboxPost fieldset span.button.postMessage').bind('click', function(e){
		socket.emit('chatbox:post', $('.chatboxPost fieldset input[name="message"]').val())
		$('.chatboxPost fieldset input[name="message"]').val('');
		return false;
	});

	// Show userlist
	$('.chatboxPost .userList').bind('click', function(e){
		if($('#chatboxContainer .chatboxGuests').css('display') == 'none')
		{
			$('#chatboxContainer .chatboxGuests').css('display', 'block');
			$('#chatboxContainer .chatboxGuests').animate({ width : '282px'}, 500, function()
			{
				console.log('clicked userlist, which was hidden');
			});	
		}
		else
		{
			$('#chatboxContainer .chatboxGuests').animate({ width : '0px'}, 500, function()
			{
				$('#chatboxContainer .chatboxGuests').css('width', '');
				$('#chatboxContainer .chatboxGuests').css('display', '');
				console.log('clicked userlist, which was shown');
			});	
		}
	});

	//***** Socket functions *****//
	socket.on('chatbox-accesGranted', function()
	{
		document.location = '/';
	});

	socket.on('chatbox-accesDenied', function()
	{
		$("#alert").addClass("error").html("Oeps, er is een fout wachtwoord opgegeven.");
	});

	socket.on('chatbox-usernameTaken', function()
	{
		$("#alert").addClass("error").html("Oeps, de gebruikersnaam is al in gebruik.");
	});

	socket.on('chatbox-enteringUser', function(data)
	{	
		$('.chatboxGuests').empty();

		for(user in data.guestList)
		{
			if(data.guestList[user] != null)
				$('.chatboxGuests').prepend('<span>' + data.guestList[user] + '</span><br />');
		}
	});

	socket.on('chatbox-messageReceived', function(data)
	{
		var postDate = new Date(data.date);
		$('.chatboxMessages').append('<div class="message ' + data.direction + '"><div class="author"><div class="date">' + postDate.getDate() + '/' + postDate.getMonth() + '/' + postDate.getFullYear() + ' ' + postDate.getHours() + ':' + postDate.getMinutes() + '</div><div class="name">' + data.author + '</div><div class="clear"></div></div><div class="messageText">' + data.message + '</div></div>');
		$('.chatboxMessages').stop().animate({
		  scrollTop: $(".chatboxMessages")[0].scrollHeight
		}, 800);
	})
}

function fixSizes ()
{
	var fullSize 		= { width 	: $("#chatboxContainer").width(), 
							height 	: $("#chatboxContainer").outerHeight(true) }
	var guestListSize 	= { width 	: $("#chatboxContainer .chatboxGuests").width(), 
							height 	: $("#chatboxContainer .chatboxGuests").outerHeight(true) }
	var postBoxSize 	= { width 	: $("#chatboxContainer .chatboxPost").width(), 
							height 	: $("#chatboxContainer .chatboxPost").outerHeight(true) }

	$("#chatboxContainer .chatboxMessages")	.css('width', 	(fullSize.width - (guestListSize.width + 8)));
	$("#chatboxContainer .chatboxMessages")	.css('height', 	(fullSize.height - (postBoxSize.height + 40)));
	$("#chatboxContainer .chatboxGuests")	.css('height', 	(fullSize.height - (postBoxSize.height + 20)));
	$("#chatboxContainer .chatboxGuests")	.css('width', '');
	$("#chatboxContainer .chatboxGuests")	.css('display', '');
}

$(window).bind('load resize', fixSizes);

chatbox();
fixSizes();