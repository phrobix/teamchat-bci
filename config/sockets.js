module.exports = function(app)
{
	// Request chatbox socket logic
	var chatbox = require('../app/sockets/chatbox');

	app.io.route('ready', function(req){
		console.log('Application ready');
	});

	app.io.route('chatbox', 
	{
		ready 		: chatbox.ready,
		enter 		: chatbox.enter,
		post		: chatbox.post,
		setNickname : chatbox.setNickname
	});

	app.io.route('disconnect', chatbox.leave);
}