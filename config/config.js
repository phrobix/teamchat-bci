var fs = require('fs');
var path = require('path');
var root = path.normalize(__dirname + '/..');

module.exports = {
	development : {

		// database configuration
		database : 'mongodb://localhost/teamchat',

		// application configuration
		app : {
			name : 'TeamChat | Make a team chatbox',
			engine : 'jade',
			http : {
				enabled : true,
				port : 9283,
			},
			https : {
				enabled : false,
				port : 83921,
				options : {
				// cert : fs.readFileSync('config/ssl/certificate.pem'),
				// key : fs.readFileSync('config/ssl/privatekey.pem'),
				},
			},
			session : {
				secret : 'F4r$#R@3@d3@$D3d3F54FDAFS4$F',
				key : 'session.id'
			},
		},

		// email configuration
		email : {
			user : '',
			pass : '',
			host : 'smtp.teamchat.nl',
			options : {
				ssl : true,
			}
		},

		// path configuration
		path : {
			root : root,
			views : root + '/app/views',
			models : root + '/app/models',
			public : root + '/public',
			cache : root + '/cache'
		},

		// Site configuration
		site : {
			domain : 'teamchat.nl'
		}
	},
	production : {

		// database configuration
		database : 'mongodb://localhost/teamchat',

		// application configuration
		app : {
			name : 'TeamChat | Make a team chatbox',
			engine : 'jade',
			http : {
				enabled : true,
				port : 9283,
			},
			https : {
				enabled : false,
				port : 83921,
				options : {
				// cert : fs.readFileSync('config/ssl/certificate.pem'),
				// key : fs.readFileSync('config/ssl/privatekey.pem'),
				},
			},
			session : {
				secret : 'F4r$#R@3@d3@$D3d3F54FDAFS4$F',
				key : 'session.id'
			},
		},

		// email configuration
		email : {
			user : '',
			pass : '',
			host : 'smtp.teamchat.nl',
			options : {
				ssl : true,
			}
		},

		// path configuration
		path : {
			root : root,
			views : root + '/app/views',
			models : root + '/app/models',
			public : root + '/public',
			cache : root + '/cache'
		},

		// Site configuration
		site : {
			domain : 'teamchat.nl'
		}
	},
	test : {}
}
