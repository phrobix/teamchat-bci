

module.exports = function(app) 
{
	// Create chatbox route
	var chatbox = require('../app/controllers/chatbox');
	app.post('/create',	chatbox.createPost);
	app.get('/create', function(req, res) { res.redirect("/"); });

	// home route
	var homepage = require('../app/controllers/homepage');

	app.get('/', function(req,res)
	{ 
		splitedHost = req.headers.host.split('.');

		req.session.init = true;

		if( splitedHost.length > 2 )
		{
			var room = (splitedHost[0]);
			return chatbox.room(req, res, room);
		}
		else
			// If root then send normal index;
			return homepage.index(req, res);
	})
}
