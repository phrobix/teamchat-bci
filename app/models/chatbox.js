var mongoose = require('mongoose');

var ChatboxSchema = new mongoose.Schema({
	name : {
		required : true,
		trim : true,
		type : String,
		unique : true,
	},
	adminPassword : {
		required : true,
		type : String
	},
	chatboxOptions : {
		type : mongoose.Schema.Types.ObjectId,
		ref : 'chatboxOptions',
		required : true
	}
});

var ChatboxOptionsSchema = new mongoose.Schema({
	enterPassword : String,
	saveLogs : Boolean
});

mongoose.model('chatbox', ChatboxSchema);
mongoose.model('chatboxOptions', ChatboxOptionsSchema);