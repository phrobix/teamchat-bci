var mongoose = require('mongoose');

var messageSchema = new mongoose.Schema({
	author : {
		required : true,
		trim : true,
		type : String,
	},
	authorSession : {
		required : true,
		trim : true,
		type : String,
	},
	date : {
		required : true,
		type : Date,
	},
	message : {
		required : true,
		trim : true,
		type : String,
	},
	chatboxObject : {
		type : mongoose.Schema.Types.ObjectId,
		ref : 'chatbox',
		required : true
	}
});

mongoose.model('messages', messageSchema);