/*
 * requirements
 */
var mongoose 		= require('mongoose');
var Chatbox 		= mongoose.model('chatbox');
var ChatboxOptions 	= mongoose.model('chatboxOptions');

/*
 * logic
 */
exports.room = function(req, res, room){
	console.log("load room: " + room);
	req.session.chatbox = room;

	Chatbox.find({ name : room }).populate('chatboxOptions').exec(function(err,result)
	{
		if(err)
			console.log('err', err);
		else
		{
			if(result.length < 1)
			{
				res.render('homepage/index', 
				{ 
					chatbox : 	{ name : room } 
				});
			}
			else
			{
				if ((!result[0]['chatboxOptions'].enterPassword) || (req.session.accessTo && req.session.accessTo[room]))
				{
					if(req.session.userName && req.session.userName.trim())
					{
						res.render('chatbox/chatbox', 
						{ 
							layout 	: 	{ 	headerHidden 	: true, 
											footerHidden 	: true, 
											fullContainer 	: true,
											noBackground	: true
										}, 
							chatbox : 	{ 	name 		: room, 
											options 	: result[0]['chatboxOptions'] }, 
							user 	: 	{ 	accesTo 	: req.session.accessTo,
											userName	: req.session.userName } 
						});
					}
					else
					{
						res.render('chatbox/setNickname', { chatbox : { name : room }});
					}
				}
				else
				{
					res.render('chatbox/locked',
					{
						chatbox : 	{ name 			: room }
					})
				}
			}
		}
	});
}

exports.create = function(req,res) {
	res.render("chatbox/create");
}
exports.createPost = function(req, res) {
	var post = req.body;

	if(post.chatboxName && post.chatboxName != '')
	{
		Chatbox.findOne({ name : post.chatboxName }, function(err, chatbox)
		{
			if(chatbox == null)
			{
				if(post.adminPassword && post.adminPassword != '')
				{
					// Create new chatbox
					new ChatboxOptions(
					{
						enterPassword 	: post.chatboxPassword,
						saveLogs 		: ((post.saveLogs == 'on') ? true : false)
					})
					.save(function(err, result)
					{
						if(err)
							console.log('64', err);

						var optionId = result['_id'];

						new Chatbox(
						{
							name 			: post.chatboxName,
							adminPassword 	: post.adminPassword,
							chatboxOptions 	: (optionId)
						})
						.save(function(err)
						{
							if(err)
								console.log('77', err);
							else
								res.render("chatbox/created", post);
						})
					});
				}
				else
					res.render("chatbox/create", { error: { type: "error", message : "Admin password can't be empty" }, chatbox : { name : post.chatboxName } });
			}
			else
				res.render("chatbox/create", { error: { type: "error", message : "Chatbox already exists" } });
		});	
	}
	else
		res.render("chatbox/create", { error: { type: "error", message : "Chatbox name was empty" } });
} 