/* Sockets logic */
var socketChatrooms = {},
	nicknames = {},
	five_minutes = 1000*30;

exports.ready = function(req)
{
	console.log('Chatbox ready');

	// Remove inactive usernames!
	for(user in nicknames)
	{
		if(nicknames[user].active == false)
		{
			var nowDate = new Date().getTime();
			var userDate = nicknames[user].timer.getTime();

			var difference_ms = nowDate - userDate;
			if(difference_ms > five_minutes)
			{
				nicknames[user].remove = true;
			}
		}
	}

	var user = req.session.userName;
	var mongoose 		= require('mongoose');
	var Chatbox 		= mongoose.model('chatbox');
	var ChatboxOptions 	= mongoose.model('chatboxOptions');
	var Messages		= mongoose.model('messages');

	if(nicknames[user])
	{
		if(nicknames[user].remove == true)
		{
			if(socketChatrooms && socketChatrooms[req.session.chatbox])
			{
				if(socketChatrooms[req.session.chatbox].guestList.indexOf(req.session.userName) == -1)
					console.log('Left, not in chatbox no prob');
				else
				{
					socketChatrooms[req.session.chatbox].guestList.splice(socketChatrooms[req.session.chatbox].guestList.indexOf(req.session.userName), 1);

					req.io.room(req.session.chatbox).broadcast('chatbox-enteringUser', 
					{ 
						user 		: req.sessionID, 
						guestList 	: socketChatrooms[req.session.chatbox].guestList 
					});
				}
			}

			req.session.userName = null;
			req.session.save();
			delete nicknames[user];
			req.io.emit('chatbox-accesGranted');
		}
		else
			if(nicknames[user].active == false)
				nicknames[user] = { active : true, timer : null };
	}

	Chatbox.find({ name : req.session.chatbox }).populate('chatboxOptions').exec(function(err,result)
	{
		if(err)
			console.log(err);
		else
		{
			if ((result[0] && (!result[0]['chatboxOptions'] || !result[0]['chatboxOptions'].enterPassword)) || (req.session.accessTo && req.session.accessTo[req.session.chatbox]))
			{
				if(socketChatrooms && socketChatrooms[req.session.chatbox])
					if(socketChatrooms[req.session.chatbox].guestList.indexOf(user) == -1)
						socketChatrooms[req.session.chatbox].guestList.push(user);
					else
						console.log('Already in chatbox');
				else
				{
					socketChatrooms[req.session.chatbox] = { guestList : new Array() }; 
					socketChatrooms[req.session.chatbox].guestList.push(user);
				}

				if((result[0] && (!result[0]['chatboxOptions'] || result[0]['chatboxOptions'].saveLogs)))
				{
					console.log('savelogs is true, send messages');
					Messages.find({ chatboxObject : result[0]._id }).exec(function(err, messages)
					{
						if(err)
							console.log(err);
						else
						{
							if(result.length > 0)
							{
								for(message in messages)
								{
									req.io.emit('chatbox-messageReceived', {
										message 	: messages[message].message,
										author 		: messages[message].author,
										date		: messages[message].date,
										direction	: (messages[message].authorSession == req.sessionID) ? 'out' : 'in'
									});
								}
							}
						}
					});
				}

				console.log('enter room and broadcast ' + req.session.chatbox);
				
				req.io.join(req.session.chatbox);

				req.io.room(req.session.chatbox).broadcast('chatbox-enteringUser', 
				{ 
					user 		: user, 
					guestList 	: socketChatrooms[req.session.chatbox].guestList 
				});

				req.io.emit('chatbox-enteringUser', 
				{
					user 		: user, 
					guestList 	: socketChatrooms[req.session.chatbox].guestList  
				});
			}
		}
	});
}

exports.leave = function(req)
{
	if(req.session.userName)
		nicknames[req.session.userName] = { active : false, timer : new Date() };

	if(socketChatrooms && socketChatrooms[req.session.chatbox])
	{
		if(socketChatrooms[req.session.chatbox].guestList.indexOf(req.session.userName) == -1)
			console.log('Left, not in chatbox no prob');
		else
		{
			socketChatrooms[req.session.chatbox].guestList.splice(socketChatrooms[req.session.chatbox].guestList.indexOf(req.session.userName), 1);

			req.io.room(req.session.chatbox).broadcast('chatbox-enteringUser', 
			{ 
				user 		: req.sessionID, 
				guestList 	: socketChatrooms[req.session.chatbox].guestList 
			});
		}
	}
}

exports.enter = function(req)
{
	if( req.session.chatbox )
	{
		var mongoose 		= require('mongoose');
		var Chatbox 		= mongoose.model('chatbox');
		var ChatboxOptions 	= mongoose.model('chatboxOptions');
		
		console.log("Trying to enter chatbox '" + req.session.chatbox + "' with password: " + req.data);

		Chatbox.find({ name : req.session.chatbox }).populate('chatboxOptions').exec( function( err, result )
		{
			if(err)
				console.log('err', err);
			else
			{
				var password = result[0]['chatboxOptions']['enterPassword'];

				if(req.data == password)
				{
					
					if(!req.session.accessTo)
						req.session.accessTo = {};

					req.session.accessTo[req.session.chatbox] = true;
					req.session.save();

					req.io.emit('chatbox-accesGranted');
				}
				else
				{
					console.log("Hahahaha, geen juist wachtwoord idioot!");
					req.io.emit('chatbox-accesDenied');
				}
			}
		});
	}
	else
		console.log("WTF!!?");
}

exports.post = function(req)
{
	var mongoose 		= require('mongoose');
	var Chatbox 		= mongoose.model('chatbox');
	var ChatboxOptions 	= mongoose.model('chatboxOptions');
	var Messages		= mongoose.model('messages');

	Chatbox.find({ name : req.session.chatbox }).populate('chatboxOptions').exec(function(err,result)
	{
		if((result[0] && (!result[0]['chatboxOptions'] || result[0]['chatboxOptions'].saveLogs)))
		{
			console.log('savelogs is true, save message');
			new Messages({
				author 			: req.session.userName, 
				authorSession 	: req.sessionID,
				message 		: req.data,
				date 			: (new Date()).toLocaleString(),
				chatboxObject	: result[0]._id
			})
			.save(function(err, response){
				if(err)
					console.log(err);
				else
					console.log('message saved');
			});
		}

		req.io.room(req.session.chatbox).broadcast('chatbox-messageReceived', {
			message 	: req.data,
			author 		: req.session.userName,
			date		: (new Date()).toLocaleString(),
			direction	: 'in'
		});
		req.io.emit('chatbox-messageReceived', {
			message 	: req.data,
			author 		: req.session.userName,
			date		: (new Date()).toLocaleString(),
			direction	: 'out'
		});

	});
}

exports.setNickname = function(req)
{
	// Remove inactive usernames!
	for(user in nicknames)
	{
		if(nicknames[user].active == false)
		{
			var nowDate = new Date().getTime();
			var userDate = nicknames[user].timer.getTime();

			var difference_ms = nowDate - userDate;
			if(difference_ms > five_minutes)
			{
				nicknames[user].remove = true;
			}
		}
	}

	if(!nicknames[req.data.trim()] || (nicknames[req.data.trim()].remove && nicknames[req.data.trim()].remove == true))
	{
		req.session.userName = req.data.trim();
		req.session.save();
		nicknames[req.data.trim()] = { active : true, timer : false };
		req.io.emit('chatbox-accesGranted');	
	}
	else
	{
		req.io.emit('chatbox-usernameTaken');
	}
}
