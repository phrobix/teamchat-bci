exports.showError = function(req, res, err, handling)
{
	if(handling[err])
	{
		//req.flash('danger', handling[err].message);
		res.redirect(handling[err].link);
	}
	else
	{
		////req.flash('danger', 'Er is een onbekende fout opgetreden.');
		res.redirect("/");
	}
}

exports.dbError = function(req, res, err, handling)
{
	if(err.name == 'MongoError')
	{
		if(err.code == 11000)
		{
			if(handling['exists'])
			{
				message = handling['exists'].message;
				//message = message.replace(/{field}/gi, err.errors.name.path);
				
				//req.flash('danger', message);
				res.redirect(handling['exists'].link);
			}
			else
			{
				//req.flash('danger', 'Er is een onbekende database fout opgetreden.');
				res.redirect("/");
			}
		}
	}
	else
	{
		var firstError;
		for(var key in err.errors) { firstError = key; break; }
		if(handling[err.errors[firstError].type])
		{
			message = handling[err.errors[firstError].type].message;
			message = message.replace(/{field}/gi, (function(){
				var fields = '';
				var numberOfErrors = err.errors.length;
				var counter = 0;

				for(error in err.errors)
				{
					if(err.errors[error])
					{
						if(counter == 0)
							fields += err.errors[error].path;
						else
							fields += ', ' + err.errors[error].path;

						counter++;
					}
				}
				return fields;
			}()));

			console.log(err.errors);
			
			//req.flash('danger', message);
			res.redirect(handling[err.errors[firstError].type].link);
		}
		else
		{
			//req.flash('danger', 'Er is een onbekende database fout opgetreden.');
			res.redirect("/");
		}
	}
}