/*var hash = require('../hash').hash;
var mongoose = require('mongoose');
var User = mongoose.model('user');
var ip = require('ip');

exports.userAuthenticate = function(email, pass, fn) {
	User.findOne({
		email : email
	}, function(err, user) {
		if (user) {
			if (err) return fn(new Error('Cannot find user.'));
			hash(pass, user.salt, function(err, hash) {
				if (err) return fn(err);
				if (hash == user.hash) return fn(null, user);
				fn(new Error('Invalid password.'));
			});
		} else {
			return fn(new Error('Cannot find user.'));
		}
	});
}

exports.userExist = function(req, res, next) {
	User.count({
		email : req.body.email
	}, function(err, count) {
		if (count === 0) {
			next();
		} else {
			req.flash('danger', 'Dit e-mail adres is al in gebruik.');
			res.redirect("/user/register");
		}
	});
}

exports.userRequiredAuthentication = function(req, res, next) {
	if (req.session.user) {
		next();
	} else {
		req.flash('danger', 'Geen toegang.');
		res.redirect('/user/login');
	}
}

exports.permissionLevel = function(req, res, next) {
	exports.userRequiredAuthentication(req, res, function() {
		User.findOne({
			email : req.session.user.email
		}).select('permissions name').populate('permissions', 'level').exec(function(err, user) {
			if (!user.permissions)
				next(1);
			else
				next(user.permissions.level);
		});
	});
}

exports.isAdmin = function(req, res, next) {
	exports.permissionLevel(req, res, function(level) {
		if (level == 2)
			next();
		else {
			req.flash('danger', 'Niet de juiste permissies.');
			res.redirect('/project');
		}
	})
}

exports.isUser = function(req, res, next) {
	exports.permissionLevel(req, res, function(level) {
		if (level == 1)
			next();
		else {
			req.flash('danger', 'Niet de juiste permissies.');
			res.redirect('/project');
		}
	})
}

exports.onlyLocal = function(req, res, next) {
	exports.userRequiredAuthentication(req, res, function() {
		var local = false;

		for ( var i = 0, max = 255; i <= max; i++) {
			if (ip.isEqual(ip.address(), '192.168.201.' + i)) local = true;
		}

		if (local == true) {
			next();
		} else {
			req.flash('danger', 'Geen toegang vanwege restrictie.');
			res.redirect('/project');
		}
	});
}*/