/**
 * Configuration require
 */
var env = process.env.NODE_ENV || 'development';
var config = require('./config/config.js')[env];

console.log(env, config);

/**
 * Module require
 */
var module;
module.express 	= require('express.io');
module.mongoose = require('mongoose');
//module.uglify 	= require('uglifyjs-middleware');
module.session	= require('express-session');
//module.less 	= require('less-middleware');
module.minify	= require('express-minify');
module.fs 		= require('fs');
module.ip 		= require('ip');

/**
 * Mongoose connection
 */
module.mongoose.connect(config.database);

/**
 * Model require
 */
module.fs.readdirSync(config.path.models).forEach(function(file) {
	if (~file.indexOf('.js')) require(config.path.models + '/' + file);
});

/**
 * Initialize Express
 */
var app = module.express().http().io();

require('./config/express')(app, config, module)
require('./config/routes')(app, module);
require('./config/sockets')(app, module);

/**
 * Create server(s)
 */
console.log('Express is binding to ' + module.ip.address());
module.mongoose.connection.on('open', function() {
	app.listen(config.app.http.port, function(){
	   console.log("Express server listening on port " + config.app.http.port);
	});
});

app.io.configure(function() {
    app.io.enable('browser client minification');  // send minified client
    app.io.enable('browser client gzip');          // gzip the file
    //app.io.set('log level', 1);                    // reduce logging
})


module.mongoose.connection.on('error', function(err) {
	if (err) console.log(err);
});

// expose app
exports = module.exports = app
